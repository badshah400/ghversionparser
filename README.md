# GHVersionParser #

A collection of utilities used to track and compare package versions on the [open Build Service](https://build.opensuse.org/users/badshah400) against the latest version available upstream. Useful for openSUSE package maintainers.

Started out as a tool to track packages with upstream repositories in GitHub — hence the name — but now has separate scripts to track upstream versions from GitLab, HepForge, PyPI, and Sourceforge projects as well.


